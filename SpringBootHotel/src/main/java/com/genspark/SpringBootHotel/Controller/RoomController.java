package com.genspark.SpringBootHotel.Controller;

import com.genspark.SpringBootHotel.Entity.Room;
import com.genspark.SpringBootHotel.Service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
public class RoomController {
    @Autowired
    RoomService roomService;

    @GetMapping("/rooms")
    public List<Room> findallRooms()
    {
        return roomService.findAllRooms();
    }
    @GetMapping("/rooms/available")
    public List<Room> findallAvailableRooms()
    {
        return (List<Room>) roomService.findByAvailableTrue();
    }
    @GetMapping("/rooms/{id}")
    public Room findRoomById(@PathVariable int id)
    {
        return roomService.findRoomById(id);
    }
    @PostMapping("/rooms")
    public Room addRoom(@RequestBody Room room)
    {
        return roomService.addRoom(room);
    }
    @PutMapping("/rooms")
    public Room updateRoom(@RequestBody Room room)
    {
        return roomService.updateRoom(room);
    }
    @DeleteMapping("/rooms/{id}")
    public String DeleteRoomById(@PathVariable int id)
    {
        return roomService.deleteRoomById(id);
    }
}
