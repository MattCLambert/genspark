package com.genspark.SpringBootHotel.Controller;

import com.genspark.SpringBootHotel.Entity.Person;
import com.genspark.SpringBootHotel.Service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonController {
    @Autowired
    PersonService personService;

    @GetMapping("/people")
    public List<Person> findAllPeople()
    {
        return personService.findAllPeople();
    }

    @GetMapping("/people/{id}")
    public Person findPersonById(@PathVariable int id)
    {
        return personService.findPersonById(id);
    }
    @PostMapping("/people")
    public Person addPerson(@RequestBody Person person)
    {
        return personService.addPerson(person);
    }
    @PutMapping("/people")
    public Person updatePerson(@RequestBody Person person)
    {
        return personService.updatePerson(person);
    }
    @PutMapping("/people/{person_id}/book/{room_id}")
    public Person bookRoom(@PathVariable int person_id,@PathVariable int room_id)
    {
        return personService.bookRoom(person_id,room_id);
    }
    @PutMapping("/people/{id}/checkout")
    public String checkout(@PathVariable int id)
    {
        return personService.checkOut(id);
    }
    @DeleteMapping("/people/{id}")
    public String DeletePersonById(@PathVariable int id)
    {
        return personService.deletePersonById(id);
    }
}
