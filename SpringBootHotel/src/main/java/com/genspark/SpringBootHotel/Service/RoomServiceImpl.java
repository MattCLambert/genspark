package com.genspark.SpringBootHotel.Service;

import com.genspark.SpringBootHotel.Dao.RoomRepository;
import com.genspark.SpringBootHotel.Entity.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RoomServiceImpl implements RoomService{

    @Autowired
    RoomRepository roomRepository;

    @Override
    public List<Room> findAllRooms() {
        return roomRepository.findAll();
    }

    @Override
    public Room findRoomById(int id) {
        return roomRepository.findById(id).get();
    }

    @Override
    public Room addRoom(Room room) {
        return roomRepository.save(room);
    }

    @Override
    public Room updateRoom(Room room) {
        return roomRepository.save(room);
    }

    @Override
    public String deleteRoomById(int id) {
        roomRepository.deleteById(id);
        return "Deleted";
    }

    @Override
    public Iterable<Room> findByAvailableTrue() {
        return roomRepository.findByAvailableTrue();
    }
}
