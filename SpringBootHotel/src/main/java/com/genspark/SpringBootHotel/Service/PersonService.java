package com.genspark.SpringBootHotel.Service;

import com.genspark.SpringBootHotel.Entity.Person;
import com.genspark.SpringBootHotel.Entity.Room;

import java.util.List;

public interface PersonService {
    List<Person> findAllPeople();
    Person findPersonById(int id);
    Person addPerson(Person person);
    Person updatePerson(Person person);
    String deletePersonById(int id);
    Person bookRoom(int person_id,int room_id);
    String checkOut(int id);

}
