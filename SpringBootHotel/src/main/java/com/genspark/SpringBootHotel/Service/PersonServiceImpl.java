package com.genspark.SpringBootHotel.Service;

import com.genspark.SpringBootHotel.Dao.PersonRepository;
import com.genspark.SpringBootHotel.Dao.RoomRepository;
import com.genspark.SpringBootHotel.Entity.Person;
import com.genspark.SpringBootHotel.Entity.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PersonServiceImpl implements PersonService{
    @Autowired
    PersonRepository personRepository;
    @Autowired
    RoomRepository roomRepository;
    @Override
    public List<Person> findAllPeople() {
        return personRepository.findAll();
    }

    @Override
    public Person findPersonById(int id) {
        return personRepository.findById(id).get();
    }

    @Override
    public Person addPerson(Person person) {
        return personRepository.save(person);
    }

    @Override
    public Person updatePerson(Person person) {
        return personRepository.save(person);
    }

    @Override
    public String deletePersonById(int id) {
        checkOut(id);
        personRepository.deleteById(id);
        return "Deleted";
    }

    @Override
    public Person bookRoom(int person_id,int room_id) {
        Person person = personRepository.findById(person_id).get();
        checkOut(person_id);
        Room room = roomRepository.findById(room_id).get();
        if(room==null||person==null)
        {
            return null;
        }
        if(room.isAvailable()&&room.getPrice()<person.getBudget())
        {
            person.setRoom(room);
            room.setAvailable(false);
            roomRepository.save(room);
            personRepository.save(person);

        }
        return person;
    }

    @Override
    public String checkOut(int id) {
        Person person = personRepository.findById(id).get();
        if(person==null || person.getRoom()==null)
        {
            return "No reservation made";
        }

        Room room = person.getRoom();
        room.setAvailable(true);
        roomRepository.save(room);
        person.setRoom(null);
        person.setBudget(person.getBudget()-room.getPrice());
        personRepository.save(person);
        return "Checked out of room " + room.getName();
    }

}
