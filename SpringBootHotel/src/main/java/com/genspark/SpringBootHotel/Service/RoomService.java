package com.genspark.SpringBootHotel.Service;

import com.genspark.SpringBootHotel.Entity.Room;

import java.util.List;

public interface RoomService{
    List<Room> findAllRooms();
    Room findRoomById(int id);
    Room addRoom(Room room);
    Room updateRoom(Room room);
    String deleteRoomById(int id);
    Iterable<Room> findByAvailableTrue();
}
