package com.genspark.SpringBootHotel.Dao;

import com.genspark.SpringBootHotel.Entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface RoomRepository extends JpaRepository<Room,Integer> {
    Iterable<Room> findByAvailableTrue();
}
