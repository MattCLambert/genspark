package xmlVersion;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("xmlDependencyInjection.xml");
        Student student = (Student) context.getBean("Student");
        System.out.println(student);
    }
}
