import java.util.ArrayList;

public class Human extends Humanoid{
    private ArrayList<Item> inventory;
    private int armor;
    public Human() {
        super();
        setSymbol('H');
        inventory = new ArrayList<>();
    }

    public Human(int health, int damage, int moveSpeed) {
        super(health, damage, moveSpeed);
        this.setRegen(1);
        this.setSymbol('H');
        inventory = new ArrayList<>();
    }
    public void addItem(Item i)
    {
        inventory.add(i);
        switch (i.getModifier())
        {
            case Armor -> setArmor(getArmor()+i.getStatBuff());
            case Speed -> setMaxMoveSpeed(getMaxMoveSpeed()+i.getStatBuff());
            case Damage -> setDamage(getDamage()+i.getStatBuff());
            case Health -> setHealth(getHealth()+i.getStatBuff());
        }
    }
    public ArrayList<Item> getInventory() {
        return inventory;
    }

    public void setInventory(ArrayList<Item> inventory) {
        this.inventory = inventory;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    @Override
    public String toString() {
        return "I have "+getMaxHealth()+" Max Health "+getHealth()+ " Current Health " + getArmor() +" Armor\n" +
                getMaxMoveSpeed()+" Speed "+getDamage() + " Damage";
    }
}
