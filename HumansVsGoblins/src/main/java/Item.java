public class Item {
    private int statBuff;
    private Modifiers modifier;

    public Item() {
    }

    public Item(int statBuff, Modifiers modifier) {
        this.statBuff = statBuff;
        this.modifier = modifier;
    }

    public int getStatBuff() {
        return statBuff;
    }

    public void setStatBuff(int statBuff) {
        this.statBuff = statBuff;
    }

    public Modifiers getModifier() {
        return modifier;
    }

    public void setModifier(Modifiers modifier) {
        this.modifier = modifier;
    }
}
