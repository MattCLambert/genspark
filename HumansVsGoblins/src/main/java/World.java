import java.util.ArrayList;
import java.util.Arrays;

public class World {
    private Land[][] world;
    private ArrayList<Treasure> treasures;
    public World() {
        this.treasures = new ArrayList<>();
    }
    public World(Land[][] world) {
        this.world = world;
        this.treasures = new ArrayList<>();
    }

    public void generateWorld(int rows, int columns)
    {
        this.world = new Land[rows][columns];
        for(int i=0;i<rows;i++)
        {
            Landtypes type;
            for(int j=0;j<columns;j++)
            {
                int rand = (int)Math.floor(Math.random()*100);
                if(rand<10)
                {
                    type = Landtypes.Water;
                }
                else if(rand<30)
                {
                    type = Landtypes.Hill;
                }
                else
                {
                    type= Landtypes.Grass;
                }
                this.world[i][j] = new Land(type);
            }
        }
    }
    public void moveHumanoid(Humanoid humanoid,String direction)
    {
        int destinationX = humanoid.getRow();
        int destinationY = humanoid.getCol();
        if(direction.toUpperCase().equals("N"))
        {
            destinationX = humanoid.getRow()-1;
        }
        else if(direction.toUpperCase().equals("W"))
        {
            destinationY = humanoid.getCol()-1;
        }
        else if(direction.toUpperCase().equals("E"))
        {
            destinationY = humanoid.getCol()+1;
        }
        else if(direction.toUpperCase().equals("S"))
        {
            destinationX = humanoid.getRow()+1;
        }
        else if(direction.toUpperCase().equals("X"))
        {
            humanoid.setMoveSpeed(0);
        }
        if(destinationX == humanoid.getRow() && destinationY == humanoid.getCol())
        {
            return;
        }
        else
        {
            world[humanoid.getRow()][humanoid.getCol()].removeEntity(humanoid);
            if(world[destinationX][destinationY].getSymbol()=='T')
            {
                Treasure treasure = (Treasure)world[destinationX][destinationY].getEntities().get(0);
                Item item = treasure.getLoot() ;
                if(humanoid instanceof Human)
                {
                    ((Human) humanoid).addItem(item);
                    System.out.println("Your " + item.getModifier() + " has been upgraded by "+item.getStatBuff());
                    world[destinationX][destinationY].removeEntity(treasure);

                }
                else if(humanoid instanceof Goblin)
                {
                    world[destinationX][destinationY].removeEntity(treasure);
                }
                treasures.remove(treasure);
            }
            world[destinationX][destinationY].placeEntity(humanoid);
            humanoid.setRow(destinationX);
            humanoid.setCol(destinationY);

        }


        if(world[humanoid.getRow()][humanoid.getCol()].getType() == Landtypes.Hill)
        {
            humanoid.setMoveSpeed(humanoid.getMoveSpeed()-2);
        }
        else if(world[humanoid.getRow()][humanoid.getCol()].getType() == Landtypes.Water)
        {
            humanoid.setMoveSpeed(humanoid.getMoveSpeed()-3);
        }
        else
        {
            humanoid.setMoveSpeed(humanoid.getMoveSpeed()-1);
        }
    }
    public String moveHumanoidGUI(Humanoid humanoid,String direction)
    {
        String ret="";
        int destinationX = humanoid.getRow();
        int destinationY = humanoid.getCol();
        if(direction.toUpperCase().equals("N"))
        {
            destinationX = humanoid.getRow()-1;
        }
        else if(direction.toUpperCase().equals("W"))
        {
            destinationY = humanoid.getCol()-1;
        }
        else if(direction.toUpperCase().equals("E"))
        {
            destinationY = humanoid.getCol()+1;
        }
        else if(direction.toUpperCase().equals("S"))
        {
            destinationX = humanoid.getRow()+1;
        }
        else if(direction.toUpperCase().equals("X"))
        {
            humanoid.setMoveSpeed(0);
        }
        if(destinationX == humanoid.getRow() && destinationY == humanoid.getCol())
        {
            return "";
        }
        else
        {

            world[humanoid.getRow()][humanoid.getCol()].removeEntity(humanoid);
            if(world[destinationX][destinationY].getSymbol()=='T')
            {
                Treasure treasure = (Treasure)world[destinationX][destinationY].getEntities().get(0);
                Item item = treasure.getLoot() ;
                if(humanoid instanceof Human)
                {
                    ((Human) humanoid).addItem(item);

                    world[destinationX][destinationY].removeEntity(treasure);
                   ret =  ("Your " + item.getModifier() + " has been upgraded by "+item.getStatBuff());

                }
                else if(humanoid instanceof Goblin)
                {
                    world[destinationX][destinationY].removeEntity(treasure);
                }
                treasures.remove(treasure);
            }
            world[destinationX][destinationY].placeEntity(humanoid);
            humanoid.setRow(destinationX);
            humanoid.setCol(destinationY);

        }


        if(world[humanoid.getRow()][humanoid.getCol()].getType() == Landtypes.Hill)
        {
            humanoid.setMoveSpeed(humanoid.getMoveSpeed()-2);
        }
        else if(world[humanoid.getRow()][humanoid.getCol()].getType() == Landtypes.Water)
        {
            humanoid.setMoveSpeed(humanoid.getMoveSpeed()-3);
        }
        else
        {
            humanoid.setMoveSpeed(humanoid.getMoveSpeed()-1);
        }
        return ret;
    }
    public void refreshEntity(Humanoid humanoid)
    {
        if(world[humanoid.getRow()][humanoid.getCol()].getType() == Landtypes.Water)
        {
            humanoid.setMoveSpeed(1);
        }
        else
        {
            humanoid.setMoveSpeed(humanoid.getMaxMoveSpeed());
        }
        humanoid.setHealth(humanoid.getRegen()+humanoid.getHealth());
        if(humanoid.getHealth() > humanoid.getMaxHealth())
        {
            humanoid.setHealth(humanoid.getMaxHealth());
        }
    }
    public void resolveCombat(int row,int col)
    {
        Land tile = world[row][col];

    }
    public void generateTreasure()
    {
        Treasure treasure =  new Treasure();
        Item item = generateItem();
        treasure.setLoot(item);
        int randRow;
        int randCol;
        do {
            randRow = (int)Math.floor(Math.random()*world.length);
            randCol = (int)Math.floor(Math.random()*world[0].length);

        }while(world[randRow][randCol].getEntities().size()!=0);
        world[randRow][randCol].placeEntity(treasure);
        treasure.setRow(randRow);
        treasure.setCol(randCol);
        treasures.add(treasure);
    }
    public Item generateItem()
    {
        int statModifier =1;
        int rand = (int)Math.floor(Math.random()*4);
        Modifiers modifier;
        switch (rand)
        {
            case 0:
                modifier = Modifiers.Armor;
                break;
            case 1:
                modifier = Modifiers.Health;
                break;
            case 2:
                modifier = Modifiers.Damage;
                break;
            default:
                modifier = Modifiers.Speed;
                break;
        }
        Item item = new Item(statModifier,modifier);
        return item;
    }
    @Override
    public String toString() {
        String ret ="";
        for(int i=0;i< world.length;i++)
        {
            for(int j=0;j< world[i].length;j++)
            {
                ret+=world[i][j];
            }
            ret+="\n";
        }
        return ret;
    }


    public Land[][] getWorld() {
        return world;
    }

    public void setWorld(Land[][] world) {
        this.world = world;
    }

    public ArrayList<Treasure> getTreasures() {
        return treasures;
    }

    public void setTreasures(ArrayList<Treasure> treasures) {
        this.treasures = treasures;
    }
}
