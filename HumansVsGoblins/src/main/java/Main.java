import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        new GuiMain();
        Scanner scan =  new Scanner(System.in);
        System.out.println("Welcome To Humans vs Goblins!");
        System.out.println("You need to defeat the goblins before they can defeat you.");
        System.out.println("Move using N E S W to move in the cardinal directions. Press X to stay where you are");
        System.out.println("- represents Grasslands. There is no penalty for moving across this land.");
        System.out.println("^ represents Hills. Moving here takes up one extra movement.");
        System.out.println("~ represents Water. Moving here takes up two extra movement. Ending your turn here makes you have only one movement your next turn.");
        World world =  new World();
        world.generateWorld(7,50);
        Human human = new Human(10,5,10);

        Land[][] grid  = world.getWorld();
        human.setRow(grid.length/2);
        human.setCol(grid[0].length/2);

        grid[grid.length/2][grid[0].length/2].setType(Landtypes.Grass);
        grid[grid.length/2][grid[0].length/2].placeEntity(human);
        int enemyCount =10;
        ArrayList<Goblin> enemies =  new ArrayList<>();
        int placedEnemies =0;

        while(placedEnemies!=enemyCount)
        {
            int randRow = (int)Math.floor(Math.random()*grid.length);
            int randCol = (int)Math.floor(Math.random()*grid[0].length);
            if(grid[randRow][randCol].getEntities().size()==0)
            {
                Goblin goblin = new Goblin(3,3,5);
                goblin.setRow(randRow);
                goblin.setCol(randCol);
                enemies.add(goblin);
                grid[randRow][randCol].placeEntity(goblin);
                placedEnemies++;
            }
        }
        boolean isAlive=true;

        while(isAlive&& enemyCount>0)
        {
            world.refreshEntity(human);
            System.out.println("Your Turn");
            while(human.getMoveSpeed()>0)
            {
                System.out.println(world);
                boolean isValid = false;
                String move ="";
                while (isValid==false) {
                    try {

                        System.out.println("Move in a direction. "+human.getMoveSpeed()+" move(s) left.");

                        move = scan.next();
                        if(move.toUpperCase().equals("N")&& human.getRow()==0)
                        {
                            System.out.print("Can't go north. ");
                            throw new Exception();
                        }
                        else if(move.toUpperCase().equals("S")&& human.getRow()==grid.length-1)
                        {
                            System.out.print("Can't go south. ");
                            throw new Exception();
                        }
                        else if(move.toUpperCase().equals("W")&& human.getCol()==0)
                        {
                            System.out.print("Can't go west. ");
                            throw new Exception();
                        }
                        else if(move.toUpperCase().equals("E")&& human.getCol()==grid[0].length-1)
                        {
                            System.out.print("Can't go east. ");
                            throw new Exception();
                        }
                        else if(move.toUpperCase().equals("X"))
                        {
                            System.out.println("Staying put");
                        }
                        else if(move.toUpperCase().equals("I"))
                        {
                            System.out.println(human);
                            continue;
                        }
                        isValid=true;
                    } catch (Exception e) {
                        System.out.println("Not a valid Input");
                    }
                }

                world.moveHumanoid(human, move);

            }
            System.out.println("Goblins Turn");
            for(var goblin : enemies)
            {
                world.refreshEntity(goblin);
                int x = goblin.getRow();
                int y = goblin.getCol();
                int min = (int)Math.sqrt(Math.pow((human.getRow())-x,2)+Math.pow((human.getCol())-y,2));

                Entity target = human;
                ArrayList<Treasure> treasures = world.getTreasures();
                for(int i=0;i<treasures.size();i++)
                {
                    int temp = (int)Math.sqrt(Math.pow((treasures.get(i).getRow())-x,2)+Math.pow((treasures.get(i).getCol())-y,2));
                    if(temp<=min)
                    {

                        min = temp;
                        target = treasures.get(i);
                    }
                }
                while(goblin.getMoveSpeed()>0)
                {
                    ArrayList<String> validDirection = new ArrayList<>();
                    if (goblin.getCol() < target.getCol() && grid[goblin.getRow()][goblin.getCol() + 1].getEntities().size() < 9) {
                        validDirection.add("E");
                    }
                    if (goblin.getCol() > target.getCol() && grid[goblin.getRow()][goblin.getCol() - 1].getEntities().size() < 9) {
                        validDirection.add("W");
                    }
                    if (goblin.getRow() > target.getRow() && grid[goblin.getRow() - 1][goblin.getCol()].getEntities().size() < 9) {
                        validDirection.add("N");
                    }
                    if (goblin.getRow() < target.getRow() && grid[goblin.getRow() + 1][goblin.getCol()].getEntities().size() < 9) {
                        validDirection.add("S");
                    }
                    if (validDirection.size() == 0) {
                        break;
                    }
                    int rand = (int) Math.floor(Math.random() * validDirection.size());
                    world.moveHumanoid(goblin, validDirection.get(rand));
                }
            }
            //Combat
            if(grid[human.getRow()][human.getCol()].getSymbol()=='X')
            {
                System.out.println("Combat begins.");
                int humanAttack = (int)Math.floor(Math.random()* human.getDamage());
                ArrayList<Entity> casualties = new ArrayList<>();
                for(var entity : grid[human.getRow()][human.getCol()].getEntities())
                {
                    if(entity instanceof Goblin)
                    {
                        Goblin goblin = (Goblin) entity;
                        int goblinAttack = (int)Math.floor(Math.random()*goblin.getDamage());
                        if(humanAttack>goblinAttack)
                        {
                            goblin.setHealth(goblin.getHealth()-humanAttack);
                            if(goblin.getHealth()<=0)
                            {
                                System.out.println("You did "+humanAttack+" damage to the Goblin.\nYou have slain a Goblin!");
                                casualties.add(entity);
                                enemyCount--;
                            }
                            else
                            {
                                System.out.println("You did "+humanAttack+" damage to the Goblin.");
                            }

                        }
                        else if(humanAttack<goblinAttack)
                        {
                            int damageTaken = goblinAttack-human.getArmor();
                            if(damageTaken<0)
                            {
                                damageTaken = 0;
                            }
                            human.setHealth(human.getHealth()-goblinAttack);
                            if(human.getHealth()<=0)
                            {

                                System.out.println("You took "+goblinAttack+" damage from the Goblin.\nYou have died!");
                                isAlive=false;
                            }
                            else
                            {
                                System.out.println("You took "+goblinAttack+" damage from the Goblin.");
                            }
                        }
                        else
                        {
                            System.out.println("You and the goblin are equally matched. No damage on either side");
                        }

                    }
                }
                for(var goblin: casualties)
                {
                    grid[human.getRow()][human.getCol()].removeEntity(goblin);
                    enemies.remove(goblin);
                    world.generateTreasure();
                }
            }
        }
        if(isAlive)
        {
            System.out.println("You won the game!");
        }
        else
        {
            System.out.println("You lost the game.");
        }

    }
}
