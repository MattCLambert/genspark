import java.util.ArrayList;

public class Land {
    private char symbol;
    private Landtypes type;
    private ArrayList<Entity> entities;

    public Land() {
        this.entities = new ArrayList<>();
    }

    public Land(Landtypes type) {
        this.entities = new ArrayList<>();
        setType(type);
    }

    public void placeEntity(Entity entity)
    {
        entities.add(entity);
        updateSymbol();

    }
    public void removeEntity(Entity entity)
    {
        entities.remove(entity);
        updateSymbol();
    }
    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }
    public void updateSymbol()
    {
        if(entities.size()==0)
        {
            switch (type)
            {
                case Hill:
                    setSymbol('^');
                    break;
                case Grass:
                    setSymbol('-');
                    break;
                case Water:
                    setSymbol('~');
                    break;
            }
        }
        else if(entities.size()==1)
        {
            setSymbol(entities.get(0).getSymbol());
        }
        else
        {
            for(var entity: entities)
            {
                if(entity instanceof Human)
                {
                    setSymbol('X');
                    return;
                }
            }
            setSymbol((char)(entities.size()+'0'));
        }
    }
    public Landtypes getType() {
        return type;
    }

    public void setType(Landtypes type) {
        this.type = type;
        switch (type)
        {
            case Hill:
                setSymbol('^');
                break;
            case Grass:
                setSymbol('-');
                break;
            case Water:
                setSymbol('~');
                break;
        }

    }

    @Override
    public String toString() {
        return String.valueOf(this.symbol);
    }

    public ArrayList<Entity> getEntities() {
        return entities;
    }

    public void setEntities(ArrayList<Entity> entities) {
        this.entities = entities;
    }
}
