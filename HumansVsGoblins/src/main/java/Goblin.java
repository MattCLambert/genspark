public class Goblin extends Humanoid{
    public Goblin() {
        super();
        setSymbol('G');
    }
    public Goblin(int health, int damage, int moveSpeed) {
        super(health, damage, moveSpeed);
        this.setRegen(0);
        setSymbol('G');
    }
    public void levelUp()
    {
        setMaxHealth(getMaxHealth()+1);
        setDamage(getDamage()+1);
        setMaxMoveSpeed(getMaxMoveSpeed()+1);
        this.setRegen(getRegen()+1);
    }
    @Override
    public String toString() {
        return "I am a goblin with "+getMaxHealth()+" Max Health "+getHealth()+ " Current Health\n" +
                getMaxMoveSpeed()+" Speed "+getDamage() + " Damage";
    }

}
