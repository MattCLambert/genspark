public class Entity
{

    private int row;
    private int col;
    private char symbol;
    public Entity() {
        this.row = 0;
        this.col = 0;
        this.symbol = ' ';
    }

    public Entity(int row,int col) {
        this.row = row;
        this.col = col;
        this.symbol = ' ';
    }


    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }
}
