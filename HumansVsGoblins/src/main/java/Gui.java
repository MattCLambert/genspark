import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Gui extends JFrame implements ActionListener{
    JButton northButton;
    JButton westButton;
    JButton eastButton;
    JButton southButton;
    JButton endTurnButton;
    JButton infoButton;
    JButton resetButton;
    World world;
    Human human;
    JTextArea game;
    JTextArea log;
    ArrayList<Goblin> enemies;
    Land[][] grid;
    public Gui(){

        this.setTitle("Humans vs Goblins");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(500,450);
        this.setLayout(new BorderLayout(0,0));
        this.setVisible(true);

        this.getContentPane().setBackground(new Color(0x12345));
        JPanel buttonPanel = new JPanel();
        JPanel gamePanel = new JPanel(new BorderLayout(0,0));
        gamePanel.setBackground(Color.BLACK);
        this.setBackground(Color.BLACK);
        setUp();
        String gameText = world.toString();
        game = new JTextArea(gameText);
        game.setForeground(Color.GREEN);
        game.setBackground(Color.BLACK);

        gamePanel.add(game,BorderLayout.CENTER);
        log = new JTextArea("");
        log.setForeground(Color.GREEN);
        log.setBackground(Color.BLACK);

        gamePanel.add(log,BorderLayout.SOUTH);
        this.add(gamePanel,BorderLayout.CENTER);

        northButton = new JButton("N");
        northButton.addActionListener(this);
        westButton = new JButton("W");
        westButton.addActionListener(this);
        eastButton = new JButton("E");
        eastButton.addActionListener(this);
        southButton = new JButton("S");
        southButton.addActionListener(this);
        endTurnButton = new JButton("End");
        endTurnButton.addActionListener(this);
        infoButton = new JButton("Info");
        infoButton.addActionListener(this);

        resetButton = new JButton("Reset");
        resetButton.addActionListener(this);
        resetButton.setVisible(false);

        buttonPanel.add(northButton);
        buttonPanel.add(westButton);
        buttonPanel.add(eastButton);
        buttonPanel.add(southButton);
        buttonPanel.add(endTurnButton);
        buttonPanel.add(infoButton);
        buttonPanel.add(resetButton);
        this.add(buttonPanel,BorderLayout.SOUTH);
        this.setVisible(true);
    /*
        button =  new JButton();
        button.setBounds(100,100,250,100);
        button.addActionListener(e-> System.out.println("Freedom"));
        button.setText("I am a button");
        button.setFocusable(false);
        button.setFont(new Font("Comic Sans",Font.BOLD,25));
        button.setForeground(Color.CYAN);
        button.setBackground(Color.lightGray);
        button.setBorder(BorderFactory.createEtchedBorder());

        this.setTitle("Humans vs Goblins");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(420,420);
        this.setVisible(true);

        this.getContentPane().setBackground(new Color(0x12345));
        this.add(button);

     */
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String message="";

        if(e.getSource()==resetButton)
        {
            setUp();
            northButton.setVisible(true);
            westButton.setVisible(true);
            eastButton.setVisible(true);
            southButton.setVisible(true);
            infoButton.setVisible(true);
            endTurnButton.setVisible(true);
            resetButton.setVisible(false);
        }

        if(e.getSource()==northButton)
        {
            message = world.moveHumanoidGUI(human,"N");
            southButton.setEnabled(true);
            if(human.getRow()==0)
            {
                northButton.setEnabled(false);
            }
        }
        else if(e.getSource()==westButton)
        {
            message = world.moveHumanoidGUI(human,"W");
            eastButton.setEnabled(true);
            if(human.getCol()==0)
            {
                westButton.setEnabled(false);
            }
        }
        else if(e.getSource()==eastButton)
        {
            message = world.moveHumanoidGUI(human,"E");
            westButton.setEnabled(true);
            if(human.getCol()==grid[0].length-1)
            {
                eastButton.setEnabled(false);
            }
        }
        else if(e.getSource()==southButton)
        {
            message = world.moveHumanoidGUI(human,"S");
            northButton.setEnabled(true);
            if(human.getRow()==grid.length-1)
            {
                southButton.setEnabled(false);
            }
        }
        else if(e.getSource()==infoButton)
        {
            message+=(human.toString())+"\n";
        }
        if(e.getSource()==endTurnButton)
        {

            world.moveHumanoidGUI(human,"X");
            moveEnemies();
            combat();
            if(enemies.size()>0 && human.getHealth()>0) {
                northButton.setEnabled(true);
                if (human.getRow() == 0) {
                    northButton.setEnabled(false);
                }
                westButton.setEnabled(true);
                if (human.getCol() == 0) {
                    westButton.setEnabled(false);
                }
                eastButton.setEnabled(true);
                if (human.getCol() == grid[0].length - 1) {
                    eastButton.setEnabled(false);
                }
                southButton.setEnabled(true);
                if (human.getRow() == grid.length - 1) {
                    southButton.setEnabled(false);
                }
            }
        }
        else
        {
            log.setText(message+"\nYou have "+ human.getMoveSpeed()+" moves left before the goblins take their turn.");
        }

        if(human.getMoveSpeed()<=0)
        {
            northButton.setEnabled(false);
            westButton.setEnabled(false);
            eastButton.setEnabled(false);
            southButton.setEnabled(false);
        }
        game.setText(world.toString());

    }

    public void moveEnemies()
    {
        for(var goblin : enemies)
        {
            world.refreshEntity(goblin);
            int x = goblin.getRow();
            int y = goblin.getCol();
            int min = (int)Math.sqrt(Math.pow((human.getRow())-x,2)+Math.pow((human.getCol())-y,2));

            Entity target = human;
            ArrayList<Treasure> treasures = world.getTreasures();
            for(int i=0;i<treasures.size();i++)
            {
                int temp = (int)Math.sqrt(Math.pow((treasures.get(i).getRow())-x,2)+Math.pow((treasures.get(i).getCol())-y,2));
                if(temp<=min)
                {

                    min = temp;
                    target = treasures.get(i);
                }
            }
            while(goblin.getMoveSpeed()>0)
            {
                ArrayList<String> validDirection = new ArrayList<>();
                if (goblin.getCol() < target.getCol() && grid[goblin.getRow()][goblin.getCol() + 1].getEntities().size() < 9) {
                    validDirection.add("E");
                }
                if (goblin.getCol() > target.getCol() && grid[goblin.getRow()][goblin.getCol() - 1].getEntities().size() < 9) {
                    validDirection.add("W");
                }
                if (goblin.getRow() > target.getRow() && grid[goblin.getRow() - 1][goblin.getCol()].getEntities().size() < 9) {
                    validDirection.add("N");
                }
                if (goblin.getRow() < target.getRow() && grid[goblin.getRow() + 1][goblin.getCol()].getEntities().size() < 9) {
                    validDirection.add("S");
                }
                if (validDirection.size() == 0) {
                    break;
                }
                int rand = (int) Math.floor(Math.random() * validDirection.size());
                world.moveHumanoid(goblin, validDirection.get(rand));
            }
        }
    }
    public void combat()
    {
        world.refreshEntity(human);
        String message ="";
        if(grid[human.getRow()][human.getCol()].getSymbol()=='X')
        {
            int humanAttack = (int)Math.floor(Math.random()* human.getDamage());
            ArrayList<Entity> casualties = new ArrayList<>();
            for(var entity : grid[human.getRow()][human.getCol()].getEntities())
            {
                if(entity instanceof Goblin)
                {
                    Goblin goblin = (Goblin) entity;
                    int goblinAttack = (int)Math.floor(Math.random()*goblin.getDamage());
                    if(humanAttack>goblinAttack)
                    {
                        goblin.setHealth(goblin.getHealth()-humanAttack);
                        if(goblin.getHealth()<=0)
                        {
                            message+=("You did "+humanAttack+" damage to the Goblin.\nYou have slain a Goblin!\n");
                            casualties.add(entity);
                        }
                        else
                        {
                            message+=("You did "+humanAttack+" damage to the Goblin.\n");
                        }

                    }
                    else if(humanAttack<goblinAttack)
                    {
                        int damageTaken = goblinAttack-human.getArmor();
                        if(damageTaken<0)
                        {
                            damageTaken = 0;
                        }
                        human.setHealth(human.getHealth()-goblinAttack);
                        if(human.getHealth()<=0)
                        {

                            message+=("You took "+goblinAttack+" damage from the Goblin.\nYou have died!\n");
                            log.setText(message);
                            northButton.setVisible(false);
                            westButton.setVisible(false);
                            eastButton.setVisible(false);
                            southButton.setVisible(false);
                            infoButton.setVisible(false);
                            endTurnButton.setVisible(false);
                            resetButton.setVisible(true);
                            return;
                            //isAlive=false;
                        }
                        else
                        {
                            message+=("You took "+goblinAttack+" damage from the Goblin.\n");
                        }
                    }
                    else
                    {
                        message+=("You and the goblin are equally matched. No damage on either side\n");
                    }

                }
            }
            for(var goblin: casualties)
            {
                grid[human.getRow()][human.getCol()].removeEntity(goblin);
                enemies.remove(goblin);
                world.generateTreasure();
            }
            if(enemies.size()==0)
            {
                message+=("You have Won!");
                log.setText(message);
                northButton.setVisible(false);
                westButton.setVisible(false);
                eastButton.setVisible(false);
                southButton.setVisible(false);
                infoButton.setVisible(false);
                endTurnButton.setVisible(false);
                resetButton.setVisible(true);
                return;
            }


        }
        message+="You have "+ human.getMoveSpeed()+" moves left before the goblins take their turn.";
        log.setText(message);
    }
    public void setUp()
    {
        world = new World();
        world.generateWorld(15,50);
        enemies =  new ArrayList<>();

        grid  = world.getWorld();
        human = new Human(10,5,10);
        human.setRow(grid.length/2);
        human.setCol(grid[0].length/2);

        grid[grid.length/2][grid[0].length/2].setType(Landtypes.Grass);
        grid[grid.length/2][grid[0].length/2].placeEntity(human);
        int enemyCount =10;
        int placedEnemies =0;

        while(placedEnemies!=enemyCount)
        {
            int randRow = (int)Math.floor(Math.random()*grid.length);
            int randCol = (int)Math.floor(Math.random()*grid[0].length);
            if(grid[randRow][randCol].getEntities().size()==0)
            {
                Goblin goblin = new Goblin(3,3,5);
                goblin.setRow(randRow);
                goblin.setCol(randCol);
                enemies.add(goblin);
                grid[randRow][randCol].placeEntity(goblin);
                placedEnemies++;
            }
        }


    }
}
