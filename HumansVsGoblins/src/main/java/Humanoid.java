public class Humanoid extends Entity{
    private int maxHealth;
    private int health;
    private int damage;
    private int maxMoveSpeed;
    private int moveSpeed;
    private int regen;
    public Humanoid() {
        this.health = 0;
        this.damage = 0;
        this.moveSpeed = 0;
        this.regen = 0;
    }

    public Humanoid(int health, int damage, int moveSpeed) {
        this.maxHealth = health;
        this.health = health;
        this.damage = damage;
        this.moveSpeed = moveSpeed;
        this.maxMoveSpeed = moveSpeed;
        this.regen=0;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getMoveSpeed() {
        return moveSpeed;
    }

    public void setMoveSpeed(int moveSpeed) {
        if(moveSpeed<0)
        {
            this.moveSpeed = 0;
        }
        else
        {
            this.moveSpeed = moveSpeed;
        }
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public int getMaxMoveSpeed() {
        return maxMoveSpeed;
    }

    public void setMaxMoveSpeed(int maxMoveSpeed) {
        this.maxMoveSpeed = maxMoveSpeed;
    }

    public int getRegen() {
        return regen;
    }

    public void setRegen(int regen) {
        this.regen = regen;
    }


}