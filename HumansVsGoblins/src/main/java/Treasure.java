public class Treasure extends Entity{
    private Item loot;

    public Treasure() {
        this.setSymbol('T');
    }

    public Item getLoot() {
        return loot;
    }

    public void setLoot(Item loot) {
        this.loot = loot;
    }

    @Override
    public String toString() {
        return "Treasure at " + getRow() + "," +getCol();
    }
}
