import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LandTest {
    Land land;
    @BeforeEach
    void setUp()
    {
        land = new Land(Landtypes.Grass);
    }
    @Test
    void placeEntity() {
        Human human = new Human();
        land.placeEntity(human);
        assertEquals(1,land.getEntities().size());
        assertEquals('H',land.getSymbol());
        Goblin goblin =  new Goblin();
        land.placeEntity(goblin);
        assertEquals(2,land.getEntities().size());
        assertEquals('X',land.getSymbol());
    }

    @Test
    void removeEntity() {
        Human human = new Human();
        land.placeEntity(human);
        land.removeEntity(human);
        assertEquals(0,land.getEntities().size());
        assertEquals('-',land.getSymbol());
    }

    @Test
    void updateSymbol() {
        land.setType(Landtypes.Hill);
        assertEquals('^',land.getSymbol());
    }

}