import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WorldTest {

    World world;
    @BeforeEach
    void setUp()
    {
        world = new World();
        world.generateWorld(10,10);
    }
    @Test
    void generateWorld() {
        world.generateWorld(100,100);
        assertEquals(100,world.getWorld().length);
        assertEquals(100,world.getWorld()[0].length);
    }

    @Test
    void moveHumanoid() {
        Human human = new Human();
        human.setRow(5);
        human.setCol(5);
        world.getWorld()[5][5].placeEntity(human);

        world.moveHumanoid(human,"N");


        assertEquals(0,world.getWorld()[5][5].getEntities().size());
        assertEquals(1,world.getWorld()[4][5].getEntities().size());
    }

    @Test
    void refreshEntity() {
        Human human = new Human(10,10,10);
        human.setMoveSpeed(0);
        human.setHealth(7);
        world.refreshEntity(human);
        assertEquals(10,human.getMoveSpeed());
        assertEquals(8,human.getHealth());
    }
}