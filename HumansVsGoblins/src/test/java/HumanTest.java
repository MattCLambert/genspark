import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {
    Human human;
    @BeforeEach
    void setUp()
    {
        human = new Human(10,5,5);
    }
    @Test
    void addItem() {
        Item item = new Item(1,Modifiers.Armor);
        human.addItem(item);
        assertEquals(1,human.getArmor());
        assertEquals(1,human.getInventory().size());
        assertEquals(item,human.getInventory().get(0));
    }

}