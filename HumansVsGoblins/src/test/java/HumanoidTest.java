import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanoidTest {
    Humanoid humanoid;
    @BeforeEach
    void setUp()
    {
        humanoid = new Humanoid(10,10,10);
    }
    @Test
    void getHealth() {
        assertEquals(10,humanoid.getHealth());
    }

    @Test
    void setHealth() {
        humanoid.setHealth(11);
        assertEquals(11,humanoid.getHealth());
    }

    @Test
    void getDamage() {
        assertEquals(10,humanoid.getDamage());
    }

    @Test
    void setDamage() {
        humanoid.setDamage(11);
        assertEquals(11,humanoid.getDamage());
    }

    @Test
    void getMoveSpeed() {

        assertEquals(10,humanoid.getMoveSpeed());
    }

    @Test
    void setMoveSpeed() {
        humanoid.setMoveSpeed(11);
        assertEquals(11,humanoid.getMoveSpeed());

    }

    @Test
    void getMaxHealth() {
        assertEquals(10,humanoid.getMaxHealth());
    }

    @Test
    void setMaxHealth() {
        humanoid.setMaxHealth(11);
        assertEquals(11,humanoid.getMaxHealth());
    }

    @Test
    void getMaxMoveSpeed() {
        assertEquals(10,humanoid.getMaxMoveSpeed());
    }

    @Test
    void setMaxMoveSpeed() {
        humanoid.setMaxMoveSpeed(11);
        assertEquals(11,humanoid.getMaxMoveSpeed());
    }

    @Test
    void getRegen() {
        assertEquals(0,humanoid.getRegen());
    }

    @Test
    void setRegen() {
        humanoid.setRegen(11);
        assertEquals(11,humanoid.getRegen());
    }
}