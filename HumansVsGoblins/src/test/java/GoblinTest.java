import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GoblinTest {
    Goblin goblin;
    @BeforeEach
    void setUp()
    {
        goblin = new Goblin(10,5,5);
    }
    @Test
    void levelUp() {
        goblin.levelUp();
        assertEquals(11,goblin.getMaxHealth());
        assertEquals(6,goblin.getMaxMoveSpeed());
        assertEquals(6,goblin.getDamage());
    }
}