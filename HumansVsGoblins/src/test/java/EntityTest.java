import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EntityTest {
    Entity entity;
    @BeforeEach
    void setUp()
    {
        entity = new Entity(0,0);
    }
    @Test
    void getRow() {
        assertEquals(0,entity.getRow());
    }

    @Test
    void setRow() {
        entity.setRow(1);
        assertEquals(1,entity.getRow());
    }

    @Test
    void getCol() {
        assertEquals(0,entity.getCol());
    }

    @Test
    void setCol() {
        entity.setCol(1);
        assertEquals(1,entity.getCol());
    }

    @Test
    void getSymbol() {
        assertEquals(' ',entity.getSymbol());
    }

    @Test
    void setSymbol() {
        entity.setSymbol('-');
        assertEquals('-',entity.getSymbol());
    }
}