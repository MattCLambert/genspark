package com.genspark.SpringBootStore.Controller;

import com.genspark.SpringBootStore.Entity.Product;
import com.genspark.SpringBootStore.Service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {
    @Autowired
    ProductService productService;
    @GetMapping("/products")
    public List<Product> findAllProducts()
    {
        return productService.findAllProducts();
    }
    @GetMapping("/products/{id}")
    public Product findProductById(@PathVariable int id)
    {
        return productService.findProductById(id);
    }
    @PostMapping("/products")
    public Product addProduct(@RequestBody Product product)
    {
        return productService.addProduct(product);
    }
    @PutMapping("/products")
    public Product updateProduct(@RequestBody Product product)
    {
        return productService.updateProduct(product);
    }
    @DeleteMapping("/products/{id}")
    public String deleteProductById(@PathVariable int id)
    {
        return productService.deleteProductById(id);
    }
}
