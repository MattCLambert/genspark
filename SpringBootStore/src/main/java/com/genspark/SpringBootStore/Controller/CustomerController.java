package com.genspark.SpringBootStore.Controller;

import com.genspark.SpringBootStore.Entity.Customer;
import com.genspark.SpringBootStore.Service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @GetMapping("/customers")
    public List<Customer> findAllCustomers()
    {
        return customerService.findAllCustomers();
    }
    @GetMapping("/customers/{id}")
    public Customer findCustomerById(@PathVariable int id)
    {
        return customerService.findCustomerById(id);
    }
    @PostMapping("/customers")
    public Customer addCustomer(@RequestBody Customer customer)
    {
        return customerService.addCustomer(customer);
    }
    @PutMapping("/customers")
    public Customer updateCustomer(@RequestBody Customer customer)
    {
        return customerService.addCustomer(customer);
    }
    @DeleteMapping("/customers/{id}")
    public String deleteCustomerById(@PathVariable int id)
    {
        return customerService.deleteCustomerById(id);
    }
    @PostMapping("/customers/{customer_id}/products/{product_id}")
    public String addProduct(@PathVariable int customer_id,@PathVariable int product_id)
    {
        return customerService.addProduct(customer_id,product_id);
    }
    @PutMapping("/customers/{customer_id}/products/{product_id}")
    public String removeProduct(@PathVariable int customer_id,@PathVariable int product_id)
    {
        return customerService.removeProduct(customer_id,product_id);
    }
    @PutMapping("/customers/{customer_id}/products/remove")
    public String removeProduct(@PathVariable int customer_id)
    {
        return customerService.removeAllProducts(customer_id);
    }
    @PutMapping("/customers/{id}/checkout")
    public String checkout(@PathVariable int id)
    {
        return customerService.checkout(id);
    }

}
