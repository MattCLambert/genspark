package com.genspark.SpringBootStore.Entity;

import javax.persistence.*;
import java.util.List;
import java.util.Map;
@Entity
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "pc_fid", referencedColumnName = "id")
    private List<Product> cart;
    private double wallet;

    public Customer() {
    }

    public Customer(String name, double wallet) {
        this.name = name;
        this.wallet = wallet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getCart() {
        return cart;
    }

    public void setCart(List<Product> cart) {
        this.cart = cart;
    }

    public double getWallet() {
        return wallet;
    }

    public void setWallet(double wallet) {
        this.wallet = wallet;
    }
}
