package com.genspark.SpringBootStore.Service;

import com.genspark.SpringBootStore.Entity.Customer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CustomerService {
    /*
    List<Person> findAllPeople();
    Person findPersonById(int id);
    Person addPerson(Person person);
    Person updatePerson(Person person);
    String deletePersonById(int id);
    Person bookRoom(int person_id,int room_id);
    String checkOut(int id);
     */
    List<Customer> findAllCustomers();
    Customer findCustomerById(int id);
    Customer addCustomer(Customer customer);
    Customer updateCustomer(Customer customer);
    String deleteCustomerById(int id);
    String addProduct(int customer_id,int product_id);
    String removeProduct(int customer_id,int product_id);
    String removeAllProducts(int customer_id);
    String checkout(int id);

}
