package com.genspark.SpringBootStore.Service;

import com.genspark.SpringBootStore.Dao.ProductDao;
import com.genspark.SpringBootStore.Entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductServiceImpl implements ProductService{
    @Autowired
    ProductDao productDao;
    @Override
    public List<Product> findAllProducts() {
        return productDao.findAll();
    }

    @Override
    public Product findProductById(int id) {
        return productDao.findById(id).get();
    }

    @Override
    public Product addProduct(Product product) {
        return productDao.save(product);
    }

    @Override
    public Product updateProduct(Product product) {
        return productDao.save(product);
    }

    @Override
    public String deleteProductById(int id) {
        productDao.deleteById(id);
        return "Deleted";
    }
}
