package com.genspark.SpringBootStore.Service;

import com.genspark.SpringBootStore.Dao.CustomerDao;
import com.genspark.SpringBootStore.Entity.Customer;
import com.genspark.SpringBootStore.Entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService{
    @Autowired
    CustomerDao customerDao;
    @Autowired
    ProductService productService;
    @Override
    public List<Customer> findAllCustomers() {
        return customerDao.findAll();
    }

    @Override
    public Customer findCustomerById(int id) {
        return customerDao.findById(id).get();
    }

    @Override
    public Customer addCustomer(Customer customer) {
        return customerDao.save(customer);
    }

    @Override
    public Customer updateCustomer(Customer customer) {
        return customerDao.save(customer);
    }

    @Override
    public String deleteCustomerById(int id) {
        customerDao.deleteById(id);
        return "Deleted";
    }

    @Override
    public String addProduct(int customer_id, int product_id) {
        Customer customer = findCustomerById(customer_id);
        Product product = productService.findProductById(product_id);
        if(product==null||customer==null)
        {
            throw new RuntimeException("Incorrect Id");
        }
        List<Product> cart = customer.getCart();
        cart.add(product);
        customer.setCart(cart);
        updateCustomer(customer);
        return "Added "+product.getName()+" to cart.";
    }

    @Override
    public String removeProduct(int customer_id, int product_id) {
        Customer customer = findCustomerById(customer_id);
        if(customer==null)
        {
            throw new RuntimeException("Incorrect Id");
        }
        List<Product> cart = customer.getCart();
        for(var item: cart)
        {
            if(item.getId()==product_id)
            {
                cart.remove(item);
                customer.setCart(cart);
                updateCustomer(customer);
                return "Removed item from cart";
            }

        }
        return "Item not in cart";

    }



    @Override
    public String removeAllProducts(int customer_id) {
        Customer customer = findCustomerById(customer_id);
        customer.setCart(new ArrayList<>());
        updateCustomer(customer);
        return "Removed all products";
    }

    @Override
    public String checkout(int id) {
        Customer customer = findCustomerById(id);
        if(customer == null)
        {
            throw new RuntimeException("Incorrect Id");
        }
        List<Product> cart = customer.getCart();
        int totalItems =cart.size();
        double totalPrice=0;
        for(var product : cart)
        {

            totalPrice += product.getPrice();
        }
        if(totalPrice>customer.getWallet())
        {
            throw new RuntimeException("Not enough money to purchase. Remove some items first");
        }
        customer.setWallet(customer.getWallet()-totalPrice);
        updateCustomer(customer);
        return "Purchased "+ totalItems+" item(s) from the store.";
    }
}
