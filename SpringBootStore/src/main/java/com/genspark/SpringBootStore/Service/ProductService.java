package com.genspark.SpringBootStore.Service;

import com.genspark.SpringBootStore.Entity.Product;

import java.util.List;

public interface ProductService {

    List<Product> findAllProducts();
    Product findProductById(int id);
    Product addProduct(Product product);
    Product updateProduct(Product product);
    String deleteProductById(int id);

}
