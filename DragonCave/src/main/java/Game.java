import java.util.Scanner;

public class Game {
    private int choice;
    public Game()
    {
        choice =0;
    }
    public Game(int choice) {
        this.choice = choice;
    }

    public int getChoice() {
        return choice;
    }
    public void setChoice(int choice) {
        this.choice = choice;
    }
    public void promptChoice() {
        Scanner scan = new Scanner(System.in);
        int choice = 0;
        while(choice==0) {
            try {
                choice = scan.nextInt();
                if(choice!=1 && choice != 2)
                {
                    throw new Exception();
                }
                setChoice(choice);
            } catch (Exception e) {
                System.out.println("You must enter a 1 or 2");
                choice =0;
                scan.nextLine();
            }
        }
    }
    public void welcome()
    {
        System.out.println("You are in a land of dragons\n" +
                "You see two caves. In one cave the dragon is friendly\n" +
                "and will share his treasure with you. The other dragon" +
                "is greedy and hungry and will eat you on sight.\n" +
                "Which cave will you go into?(1 or 2)");
    }
    public String makeResult()
    {
        if(this.choice==1)
        {
            return"You approach the cave...\n" +
                    "It is dark and spooky...\n" +
                    "A large dragon jumps out in front of you! He opens his jaws and...\n" +
                    "Gobbles you down in one bite!";
        }
        else
        {
            return"You approach the cave...\n" +
                    "It is dark and spooky...\n" +
                    "A large dragon jumps out in front of you! He opens his jaws and...\n" +
                    "Drops a large bag of gold in front of you!";
        }
    }
}
