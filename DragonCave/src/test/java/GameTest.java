import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GameTest {
    Game game;
    @BeforeEach
    void setUp() {
        game = new Game();
    }
    @Test
    void getChoice() {
        assertEquals(0,game.getChoice());
    }

    @Test
    void setChoice() {
        game.setChoice(1);
        assertEquals(1,game.getChoice());
    }

    @Test
    void makeResult() {
        game.setChoice(1);
        String correct ="You approach the cave...\n" +
                "It is dark and spooky...\n" +
                "A large dragon jumps out in front of you! He opens his jaws and...\n" +
                "Gobbles you down in one bite!";
        assertEquals(true,correct.equals(game.makeResult()));
    }
}