import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class HangmanTest {
    Hangman hangman;
    String[] arr;
    String art;
    String[] words;
    @BeforeEach
    void setUp()
    {
        hangman = new Hangman();
        try{
            Path filePath = Paths.get("src/main/resources/art.text");

            Charset charset = StandardCharsets.UTF_8;
            List<String> lines = Files.readAllLines(filePath,charset);

            arr = lines.get(0).split(" ");
            lines.remove(lines.get(0));
            art = lines.stream().reduce((acc,next)->acc+"\n"+next).orElse("");
            filePath = Paths.get("src/main/resources/words.text");
            lines = Files.readAllLines(filePath,charset);
            words = lines.toArray(String[]::new);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return;
        }

    }

    @Test
    void makeGuess() {
        hangman.setWords(words);
        hangman.setLifeSymbols(arr);
        hangman.setKeySet(Arrays.stream(arr).map(str->" ").toArray(String[]::new));

        hangman.chooseWord(0);
        hangman.makeGuess('b');
        assertEquals(1,hangman.getWrongGuesses());
        assertEquals("b",hangman.getMissedLetters());
        assertEquals("---",hangman.getUserWord());
        hangman.makeGuess('c');
        assertEquals(1,hangman.getWrongGuesses());
        assertEquals("b",hangman.getMissedLetters());
        assertEquals("c--",hangman.getUserWord());
        assertEquals(5,hangman.getScore());
    }

    @Test
    void resetGame() {
        hangman.setScore(30);
        hangman.setWrongGuesses(1);
        hangman.resetGame();
        assertEquals(0,hangman.getScore());
        assertEquals(0,hangman.getWrongGuesses());
    }

    @Test
    void getComputerWord() {
        assertEquals("",hangman.getComputerWord());
    }

    @Test
    void getGameArt() {
        hangman.setArt(art);
        hangman.setWords(words);
        hangman.setLifeSymbols(arr);
        hangman.setKeySet(arr);
        hangman.chooseWord(0);
        String fullArt ="   +----+\n" +
                "   |    |\n" +
                "   O    |\n" +
                "  /|\\   |\n" +
                "   |    |\n" +
                "  / \\   |";
        assertEquals(fullArt+"\nMissed: \n---",hangman.getGameArt());
        hangman.resetGame();
        hangman.chooseWord(0);
        hangman.makeGuess('z');
        hangman.makeGuess('a');
        fullArt= "   +----+\n" +
                "   |    |\n" +
                "   O    |\n" +
                "        |\n" +
                "        |\n" +
                "        |";
        assertEquals(fullArt+"\nMissed: z\n-a-",hangman.getGameArt());
    }

    @Test
    void setComputerWord() {
        hangman.setComputerWord("test");
        assertEquals("test",hangman.getComputerWord());

    }

    @Test
    void getUserWord() {
        assertEquals("",hangman.getUserWord());
    }

    @Test
    void setUserWord() {
        hangman.setComputerWord("test");
        assertEquals("----",hangman.getUserWord());
        hangman.setComputerWord("-----");
        assertEquals("-----",hangman.getUserWord());
    }

    @Test
    void getLives() {
        assertEquals(0,hangman.getLives());
        hangman.setLifeSymbols(arr);
        assertEquals(7,hangman.getLives());
    }

    @Test
    void setLives() {
        hangman.setLives(1);
        assertEquals(1,hangman.getLives());
    }

    @Test
    void getWrongGuesses() {
        assertEquals(0,hangman.getWrongGuesses());
    }

    @Test
    void setWrongGuesses() {
        hangman.setWrongGuesses(1);
        assertEquals(1,hangman.getWrongGuesses());
    }

    @Test
    void getLifeSymbols() {

        assertEquals(0,hangman.getLifeSymbols().length);
    }

    @Test
    void setLifeSymbols() {

        hangman.setLifeSymbols(arr);
        assertEquals(7,hangman.getLifeSymbols().length);

    }

    @Test
    void getKeySet() {
        assertEquals(0,hangman.getKeySet().length);
    }

    @Test
    void setKeySet() {
        hangman.setKeySet(arr);
        assertEquals(7,hangman.getKeySet().length);
    }

    @Test
    void getMissedLetters() {
        assertEquals("",hangman.getMissedLetters());
    }

    @Test
    void setMissedLetters() {
        hangman.setMissedLetters("test");
        assertEquals("test",hangman.getMissedLetters());
    }

    @Test
    void getArt() {
        assertEquals("",hangman.getArt());
    }

    @Test
    void setArt() {
        hangman.setArt(art);
        assertEquals(art,hangman.getArt());
    }

    @Test
    void getWords() {
        assertEquals(0,hangman.getWords().length);
    }

    @Test
    void setWords() {
        hangman.setWords(words);
        assertEquals(words.length,hangman.getWords().length);
    }

    @Test
    void chooseWord() {
        hangman.setWords(words);
        hangman.chooseWord(0);
        assertEquals("cat",hangman.getComputerWord());
        assertEquals("---",hangman.getUserWord());
    }

    @Test
    void getScore() {
        assertEquals(0,hangman.getScore());
    }

    @Test
    void setScore() {
        hangman.setScore(10);
        assertEquals(10,hangman.getScore());
    }
}