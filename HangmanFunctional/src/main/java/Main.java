import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        int lives;
        int score =0;


        Hangman hangman = new Hangman();
        Charset charset = StandardCharsets.UTF_8;
        try
        {
            Path filePath = Paths.get("src/main/resources/art.text");

            List<String> lines = Files.readAllLines(filePath,charset);
            String[] arr = lines.get(0).split(" ");
            hangman.setLifeSymbols(arr);
            String[] temp = Arrays.stream(arr).map(str->" ").toArray(String[]::new);
            hangman.setKeySet(temp);
            lines.remove(lines.get(0));
            String a = lines.stream().reduce((acc,next)->acc+"\n"+next).orElse("");
            hangman.setArt(a);
            System.out.println(String.format(a,hangman.getKeySet()));
            filePath = Paths.get("src/main/resources/words.text");
            lines = Files.readAllLines(filePath,charset);
            String[] words = lines.toArray(String[]::new);
            hangman.setWords(words);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return;
        }
        String name;
        Scanner scan = new Scanner(System.in);
        try
        {
            System.out.println("Welcome to Hangman enter your name:");
            name = scan.nextLine();
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return;
        }
        char cont;
        do {
            System.out.println("Welcome to hangman!");
            hangman.resetGame();
            hangman.play();
            hangman.setScore(hangman.getScore() + (hangman.getLives()- hangman.getWrongGuesses())*10);
            System.out.println("Your score is "+hangman.getScore());
            try {
                Scanner fileRead =  new Scanner(new File("src/main/resources/scores.text"));
                Path filePath = Paths.get("src/main/resources/scores.text");
                List<String> lines = Files.readAllLines(filePath,charset);
                int currentMax = lines.stream().map(line-> Integer.valueOf(line.split(": ")[1])).reduce((acc,next)->{
                    if(next>acc)
                    {
                        return next;
                    }
                    return acc;
                }).orElse(0);
                if(hangman.getScore()>currentMax)
                {
                    System.out.println("You have the High Score!");
                }
                else if(hangman.getScore()<currentMax)
                {
                    System.out.println("You did not beat the high score of "+ currentMax);
                }
                else
                {
                    System.out.println("You are tied for the high score");
                }
            }catch (Exception e)
            {
                System.out.println(e.getMessage());
                return;
            }
            File file;
            FileWriter fileWriter;
            PrintWriter printWriter;
            try {
                file = new File("src/main/resources/scores.text");
                fileWriter = new FileWriter(file,true);
                printWriter= new PrintWriter(fileWriter);
                printWriter.println(name +": "+hangman.getScore());
                printWriter.close();
            }catch(Exception e)
            {
                System.out.println(e.getMessage());
                return;
            }
            System.out.println("Do you want to play again?");
            cont = scan.nextLine().charAt(0);
        }while (Character.toUpperCase(cont)=='Y');

        /*
        String[] arr = new String[artLives.size()];
        Arrays.fill(arr," ");

        int wrongGuesses =0;
        String computerWord = words.get((int)Math.floor(Math.random()*words.size()));
        String userWord = Stream.of(computerWord.split("")).map(letter->"-").collect(Collectors.joining());
        while(wrongGuesses<lives)
        {

            System.out.println(String.format(art,arr));
            System.out.println("Missed: "+ missedLetters);
            System.out.println(userWord);
            System.out.println("Guess a letter");
            char guess = scan.nextLine().charAt(0);
            ***********************************
            String temp = userWord;
            userWord = IntStream.range(0,computerWord.length())
                    .mapToObj(i->{
                        if(computerWord.charAt(i)==guess)
                        {
                            return String.valueOf(computerWord.charAt(i));
                        }
                        else
                        {
                            return String.valueOf(temp.charAt(i));
                        }
                    })
                    .collect(Collectors.joining());
                    ************************************************
            if(userWord.equals(temp))
            {
                missedLetters+=guess;
                arr[wrongGuesses] = artLives.get(wrongGuesses);
                wrongGuesses++;
            }
            else
            {
                score+=5;
            }
            if(userWord.equals(computerWord))
            {
                System.out.println("Congratulations you won! The word was " + computerWord);
                break;
            }
        }
        if(wrongGuesses==lives)
        {
            System.out.println(String.format(art,arr));
            System.out.println("You have lost. The word was "+computerWord);
        }
        score+= (lives-wrongGuesses)*10;
        File file;
        FileWriter fileWriter;
        PrintWriter printWriter;
        try {
            file = new File("src/main/resources/scores.text");
            fileWriter = new FileWriter(file,true);
            printWriter= new PrintWriter(fileWriter);
            printWriter.println(name +": "+score);
            printWriter.close();
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
            return;
        }
        int max = score;
        String maxName = name;
        try
        {
            Scanner fileRead =  new Scanner(new File("src/main/resources/scores.text"));

            while(fileRead.hasNextLine())
            {
                String line = fileRead.nextLine();
                String[] temp = line.split(": ");
                System.out.println(line);
                if(Integer.valueOf(temp[1])>max)
                {
                    max = Integer.valueOf(temp[1]);
                    maxName = temp[0];
                }


            }
            fileRead.close();
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return;
        }
        if(maxName.equals(name)&&score==max)
        {
            System.out.println("You got the high score with "+score+" points.");
        }
        else
        {
            System.out.println("You did not have the high score");
        }

         */
    }
}
