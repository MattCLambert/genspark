import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Hangman {
    private String computerWord;
    private String userWord;
    private String missedLetters;
    private int lives;
    private int wrongGuesses;
    private String[] lifeSymbols;
    private String[] keySet;
    private String[] words;
    private String art;
    private Scanner scan;
    private int score;
    public Hangman() {
        this.computerWord="";
        this.userWord = "";
        this.missedLetters="";
        this.wrongGuesses = 0;
        this.lives = 0;
        this.art = "";
        this.words = new String[0];
        this.keySet = new String[0];
        this.lifeSymbols = new String[0];
        this.scan =  new Scanner(System.in);
        this.score = 0;

    }

    public void play()
    {
        System.out.println(this.getGameArt());

        System.out.println("Guess a letter: ");
        char guess;
        try{

            guess = scan.nextLine().charAt(0);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return;
        }
        makeGuess(guess);
        if(wrongGuesses>=lives)
        {
            System.out.println("Unfortunately you have lost. The word was "+computerWord);
            return;
        }
        if(userWord.equals(computerWord))
        {
            System.out.println("Congratulations you won! The word was " + computerWord);
            return;
        }
        play();
    }
    public void makeGuess(char guess)
    {
        String temp = userWord;
        this.userWord = IntStream.range(0,computerWord.length())
                .mapToObj(i->{
                    if(computerWord.charAt(i)==guess)
                    {
                        return String.valueOf(computerWord.charAt(i));
                    }
                    else
                    {
                        return String.valueOf(temp.charAt(i));
                    }
                })
                .collect(Collectors.joining());
        if(userWord.equals(temp))
        {
            missedLetters+=guess;
            keySet[wrongGuesses] = lifeSymbols[wrongGuesses];
            wrongGuesses++;
        }
        else
        {
            setScore(getScore()+5);
        }

    }
    public void resetGame()
    {
        setScore(0);
        setWrongGuesses(0);
        setMissedLetters("");
        String[] temp = Arrays.stream(getLifeSymbols()).map(str->" ").toArray(String[]::new);
        setKeySet(temp);
        int rand  = (int)Math.floor(Math.random()*words.length);
        chooseWord(rand);
    }
    public String getComputerWord() {
        return computerWord;
    }
    public String getGameArt()
    {
        return String.format(art,keySet)+"\nMissed: "+missedLetters+"\n"+userWord;
    }
    public void setComputerWord(String computerWord) {
        this.computerWord = computerWord;
        setUserWord(Stream.of(computerWord.split("")).map(letter->"-").collect(Collectors.joining()));
    }

    public String getUserWord() {
        return userWord;
    }

    public void setUserWord(String userWord) {
        this.userWord = userWord;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public int getWrongGuesses() {
        return wrongGuesses;
    }

    public void setWrongGuesses(int wrongGuesses) {
        this.wrongGuesses = wrongGuesses;
    }

    public String[] getLifeSymbols() {
        return lifeSymbols;
    }

    public void setLifeSymbols(String[] lifeSymbols) {
        this.lifeSymbols = lifeSymbols;
        setLives(lifeSymbols.length);
    }

    public String[] getKeySet() {
        return keySet;
    }

    public void setKeySet(String[] keySet) {
        this.keySet = keySet;
    }

    public String getMissedLetters() {
        return missedLetters;
    }

    public void setMissedLetters(String missedLetters) {
        this.missedLetters = missedLetters;
    }

    public String getArt() {
        return art;
    }

    public void setArt(String art) {
        this.art = art;
    }

    public String[] getWords() {
        return words;
    }

    public void setWords(String[] words) {
        this.words = words;
    }
    public void chooseWord(int index)
    {
        try{
            setComputerWord(words[index]);

        }catch(IndexOutOfBoundsException e)
        {
            System.out.println("Not a Valid index choose an index from 0-" +words.length);
            return;
        }
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
