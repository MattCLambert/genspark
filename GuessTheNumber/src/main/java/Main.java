import java.util.Scanner;

public class Main {
    private static Scanner scan;

    public static void main(String[] args)
    {
        Game game = new Game();
        String cont;
        scan =  new Scanner(System.in);
        System.out.println("Hello! What is your name?");
        boolean isValid = true;
        while(isValid)
        {
            try
            {
                game.setName(scan.nextLine());
                isValid = false;
            }
            catch(Exception e)
            {
                System.out.println(e.getMessage());

            }
        }

        do
        {
            game.setTries(6);
            game.setGuesses(0);
            game.setNum((int)(Math.random()*20+1));
            System.out.println(game.getName() +" I am thinking of a number between 1 and 20. You have six tries to guess my number.");
            int guesses=0;
            while(game.getGuesses()< game.getTries())
            {

                System.out.println("Take a guess");
                int userGuess =0;
                isValid = true;
                while(isValid)
                {
                    try
                    {
                        userGuess = scan.nextInt();
                        if(userGuess>20 || userGuess<1)
                        {
                            throw new Exception();
                        }
                        isValid = false;
                    }
                    catch(Exception e)
                    {
                        System.out.println("Invalid: Input a number between 1 and 20");
                        scan.nextLine();
                    }
                }

                String result = game.makeGuess(userGuess);
                System.out.println(result);
                if(result.equals("Good Job, "+ game.getName() +" you guessed my number in "+game.getGuesses() + " guesses! You win."))
                {
                    break;
                }
            }
            scan.nextLine();
            System.out.println("Do you want to play again?(y/n)");
            cont = scan.nextLine();
        }while(cont.toUpperCase().equals("Y"));
    }
}
