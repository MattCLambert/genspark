public class Game {
    private String name;
    private int num;
    private int tries;
    private int guesses;
    public Game()
    {
        this.name="";
        this.num=0;
        this.tries=0;
        this.guesses=0;
    }

    public String makeGuess(int userGuess)
    {
        guesses++;
        if(userGuess==num)
        {
            return"Good Job, "+ name +" you guessed my number in "+guesses + " guesses! You win.";

        }
        else if(guesses==6)
        {
            return "You ran out of guesses! I win.";
        }
        else if(userGuess<num)
        {
            return "Too low. " +(6-guesses)+" guesses left";
        }
        else
        {
            return "Too high. " +(6-guesses)+" guesses left";
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getTries() {
        return tries;
    }

    public void setTries(int tries) {
        this.tries = tries;
    }

    public int getGuesses() {
        return guesses;
    }

    public void setGuesses(int guesses) {
        this.guesses = guesses;
    }
}
