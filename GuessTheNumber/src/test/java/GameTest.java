import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
class GameTest {
    Game game;
    @BeforeEach
    void setUp() {
        game = new Game();
    }
    @Test
    void makeGuess() {
        game.setName("Matt");
        game.setNum(10);
        game.setTries(6);
        String correct="Good Job, Matt you guessed my number in 1 guesses! You win.";
        assertEquals(true,correct.equals(game.makeGuess(10)));
    }
    @Test
    void makeGuessTooHigh() {
        game.setName("Matt");
        game.setNum(10);
        game.setTries(6);
        String correct="Too high. 5 guesses left";
        assertEquals(true,correct.equals(game.makeGuess(15)));
    }
    @Test
    void makeGuessTooLow() {
        game.setName("Matt");
        game.setNum(10);
        game.setTries(6);
        String correct="Too low. 5 guesses left";
        assertEquals(true,correct.equals(game.makeGuess(5)));
    }
    @Test
    void makeGuessLose() {
        game.setName("Matt");
        game.setNum(10);
        game.setTries(6);
        String correct="You ran out of guesses! I win.";
        game.makeGuess(15);
        game.makeGuess(15);
        game.makeGuess(15);
        game.makeGuess(15);
        game.makeGuess(15);
        assertEquals(true,correct.equals(game.makeGuess(15)));
    }
    @Test
    void getName() {
        assertEquals(true,game.getName().equals(""));
    }

    @Test
    void setName() {
        game.setName("Matt");
        assertEquals(true,game.getName().equals("Matt"));
    }

    @Test
    void getNum() {
        assertEquals(0,game.getNum());
    }

    @Test
    void setNum() {
        game.setNum(10);
        assertEquals(10,game.getNum());
    }

    @Test
    void getTries() {
        assertEquals(0,game.getTries());
    }

    @Test
    void setTries() {
        game.setTries(6);
        assertEquals(6,game.getTries());
    }

    @Test
    void getGuesses() {
        assertEquals(0,game.getGuesses());
    }

    @Test
    void setGuesses() {
        game.setGuesses(6);
        assertEquals(6,game.getGuesses());
    }
}