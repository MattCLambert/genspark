import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Hangman hangman =new Hangman();
        String cont ="n";
        Scanner scan = new Scanner(System.in);
        System.out.println("Welcome to Hangman!");
        do
        {
            hangman.generateRandomWord();
            boolean didWin = false;
            while(hangman.getBadGuesses()<hangman.getLives())
            {
                System.out.println(hangman.printGallows());
                System.out.println("Missed: " + hangman.getMissed());
                System.out.println(hangman.getUserWord());
                System.out.println("Make Your Guess");
                char choice;
                try
                {
                    choice = scan.next().charAt(0);

                }
                catch(Exception e)
                {
                    System.out.println(e.getMessage());
                    continue;
                }
                didWin = hangman.makeGuess(choice);
                if(didWin)
                {
                    break;
                }
            }

            System.out.println(hangman.printGallows());
            System.out.println("Missed: " + hangman.getMissed());
            System.out.println(hangman.getUserWord());
            if(didWin)
            {
                System.out.println("Congratulations you won!");
            }
            else
            {
                System.out.println("You did not guess the word right.");
            }


            scan.nextLine();


            boolean isOkay = false;
            while (isOkay==false)
            {
                try
                {
                    System.out.println("Play again?(Y/N)");
                    cont = scan.nextLine();
                    isOkay =true;
                }
                catch (Exception e)
                {
                    System.out.println(e.getMessage());
                }
            }


        }while(cont.toUpperCase().equals("Y"));
    }
}
