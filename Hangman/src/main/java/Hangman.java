import java.util.ArrayList;

public class Hangman {

    private final int lives = 7;
    private final String dictionary[] = {
            "cat",
            "dog",
            "soldier",
            "knave",
            "king",
            "queen",
            "bishop",
            "monarchy",
            "kingdom",
            "prophecy",
            "worker",
            "production",
            "island",
            "tide",
            "navigation",
            "aristocrat",
            "money",
            "resource",
            "oligarchy",
    };
    private String userWord;
    private String computerWord;
    private int badGuesses;
    private String missed;

    public Hangman() {
        this.userWord="";
        this.badGuesses=0;
        this.missed = "";
        this.computerWord ="";
    }

    public boolean makeGuess(Character c)
    {
        String temp = "";
        int dashCount =0;
        for(int i=0;i<this.computerWord.length();i++)
        {
            if(c==this.computerWord.charAt(i))
            {
                temp+=computerWord.charAt(i);
            }
            else
            {
                if(getUserWord().charAt(i)=='_')
                {
                    dashCount++;
                }
                temp+= getUserWord().charAt(i);
            }
        }
        System.out.println(temp);
        if(!temp.equals(getUserWord()))
        {
            setUserWord(temp);

        }
        else {
            setBadGuesses(getBadGuesses() + 1);
            missed += c;
        }
        return dashCount==0;
    }
    public void chooseWord(int index)
    {
        setComputerWord(this.dictionary[index]);
        String dashes = "";
        for(int i =0;i<dictionary[index].length();i++)
        {
            dashes+="_";
        }
        setUserWord(dashes);
        setBadGuesses(0);
        setMissed("");
    }

    public String printGallows()
    {
        String arr[] = {" "," "," "," "," "," ", " "};
        if(missed.length()>=1)
        {
            arr[0]="0";
        }
        if(missed.length()>=2)
        {
            arr[1]="|";
        }
        if(missed.length()>=3)
        {
            arr[2]="|";
        }
        if(missed.length()>=4)
        {
            arr[3]="/";
        }
        if(missed.length()>=5)
        {
            arr[4]="\\";
        }
        if(missed.length()>=6)
        {
            arr[5]="/";
        }
        if(missed.length()>=7)
        {
            arr[6]="\\";
        }
        String ret = "   +----+\n" +
                     "   |    |\n" +
                     "   %1$s    |\n" +
                     "  %4$s%2$s%5$s   |\n" +
                     "   %3$s    |\n" +

                     "  %6$s %7$s   |\n";
        for(int i=1;i<=7;i++)
        {
            ret = ret.replace("%"+i+"$s",arr[i-1]);
        }
        return ret;
    }



    public String getUserWord() {
        return userWord;
    }

    public void setUserWord(String word) {
        this.userWord = word;
    }

    public int getBadGuesses() {
        return badGuesses;
    }

    public void setBadGuesses(int badGuesses) {
        this.badGuesses = badGuesses;
    }

    public String getMissed() {
        return missed;
    }

    public void setMissed(String missed) {
        this.missed = missed;
    }

    public String getComputerWord() {
        return computerWord;
    }

    public void setComputerWord(String computerWord) {
        this.computerWord = computerWord;
    }
    public void generateRandomWord()
    {
        int index = (int)Math.round(Math.random()*dictionary.length);
        chooseWord(index);
    }

    public int getLives() {
        return lives;
    }
}
