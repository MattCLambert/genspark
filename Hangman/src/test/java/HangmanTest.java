import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HangmanTest {
    Hangman hangman;
    @BeforeEach
    void setUp() {
        hangman = new Hangman();
    }
    @Test
    void makeGuessCorrect() {
        hangman.chooseWord(0);
        hangman.makeGuess('a');
        String correct= "_a_";
        assertEquals(correct,hangman.getUserWord());
    }
    void makeGuessALLIncorrect() {
        hangman.chooseWord(0);
        hangman.makeGuess('q');
        hangman.makeGuess('k');
        hangman.makeGuess('d');
        hangman.makeGuess('m');
        hangman.makeGuess('n');
        hangman.makeGuess('j');
        hangman.makeGuess('l');
        String correct= "   +----+\n" +
                "   |    |\n" +
                "   0    |\n" +
                "  /|\\   |\n" +
                "   |    |\n" +
                "  / \\   |";
        assertEquals(correct,hangman.printGallows());
    }
    @Test
    void chooseWord() {
        hangman.chooseWord(0);
        assertEquals(true,hangman.getComputerWord().equals("cat"));
    }

    @Test
    void getUserWord() {
        assertEquals(true,hangman.getUserWord().equals(""));
    }

    @Test
    void setUserWord() {
        hangman.setUserWord("test");
        assertEquals(true,hangman.getUserWord().equals("test"));
    }

    @Test
    void getBadGuesses() {
        assertEquals(0,hangman.getBadGuesses());
    }

    @Test
    void setBadGuesses() {
        hangman.setBadGuesses(1);
        assertEquals(1,hangman.getBadGuesses());
    }

    @Test
    void getMissed() {
        assertEquals(true,hangman.getMissed().equals(""));
    }

    @Test
    void setMissed() {
        hangman.setMissed("a");
        assertEquals(true,hangman.getMissed().equals("a"));
    }

    @Test
    void getComputerWord() {
        assertEquals(true,hangman.getUserWord().equals(""));
    }

    @Test
    void setComputerWord() {
        hangman.setComputerWord("a");
        assertEquals(true,hangman.getComputerWord().equals("a"));

    }
}
/*

   +----+
   |    |
   0    |
  /|\   |
   |    |
  / \   |
 */